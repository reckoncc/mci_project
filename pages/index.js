import Layout from '../components/Layout';
import MajorButtonGroup from '../components/MajorButtonGroup';
import News from '../components/News';
import LectureFailures from '../components/LectureFailures';

import { news, lectureFailures, formData } from '../components/data';

const Index = () => (
  <Layout>
    <div>
      <h1 className="display-4">Studiengang</h1>
      <br />
      <MajorButtonGroup width="550" height="100" />
      <br />
      <div className="row">
        <div className="col-md-6">
          <News list={news} />
        </div>
        <div className="col-md-6">
          <LectureFailures list={lectureFailures} />
        </div>
      </div>
    </div>
  </Layout>
);

export default Index;
