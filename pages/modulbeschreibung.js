import Layout from '../components/Layout';
import ModuleTable from '../components/ModuleTable';
import {moduleInfo} from '../components/data';

const ModuleDescription = () => (
  <Layout>
    <div>
      <h1 className="display-4">Modulbeschreibung</h1>
        <ModuleTable data={moduleInfo}/>
      </div>
  </Layout>
);

export default ModuleDescription;
