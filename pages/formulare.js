import Layout from '../components/Layout';
import FormsTable from '../components/FormsTable';

import {formData} from '../components/data';

const Formulare = (props) => (
  <Layout>
    <h1 className="display-4">Formulare</h1>
    <br />
    <div className="container">
      <FormsTable name="Bachelorarbeit/Projektarbeit" data={props.formData.arbeit} />
      <FormsTable name="Praxissemester" data={props.formData.praxis} />
      <FormsTable name="Prüfungsleistungen / Noten" data={props.formData.pruefung} />
      <FormsTable name="Studium Generale" data={props.formData.studiumGenerale} />
      <FormsTable name="Zeugnis" data={props.formData.zeugnis} />
      <FormsTable name="SPO" data={props.formData.spo} />
      <FormsTable name="Sonstiges" data={props.formData.sonstiges} />
    </div>
  </Layout>
);

Formulare.getInitialProps = () => {
  return {
    formData: formData
  }
}

export default Formulare;
