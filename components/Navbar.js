import Link from 'next/link';

const Navbar = () => (
  <nav className="navbar navbar-expand navbar-dark bg-dark mb-4">
    <div className="container">
      <Link href="/">
        <a className="navbar-brand" ><b>Info</b>rmatik</a>
      </Link>
      <div className="collapse navbar-collapse">
        <ul className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link href="/modulbeschreibung">
              <a className="nav-link">
                Modulbeschreibung
              </a>
            </Link>
          </li>
          <li className="nav-item">
            <Link href="/formulare">
              <a className="nav-link">Formulare</a>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  </nav>
);

export default Navbar;
