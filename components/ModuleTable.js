const ModuleTable = (props) => (
  <div>
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">KNr</th>
              <th scope="col">Kurs / Modul</th>
              <th scope="col">Semester</th>
              <th scope="col">CPs</th>
            </tr>
          </thead>
          <tbody>
            {
              props.data.map(item => {
                if (item.type === 'module') {
                  return (
                    <tr className="table-secondary">
                      <td><a target="_blank" href={item.url}>{item.KNr}</a></td>
                      <td colSpan="3">{item.name}</td>
                    </tr>
                  );
                } else if (item.type === 'course') {
                  return (
                    <tr>
                      <td>{item.KNr}</td>
                      <td>{item.name}</td>
                      <td>{item.semester}</td>
                      <td>{item.cp}</td>
                    </tr>
                  );
                }
              }
              )
            }
          </tbody>
        </table>
      </div>
);

export default ModuleTable;
