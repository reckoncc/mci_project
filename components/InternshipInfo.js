class InternshipInfo extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    var company;
    var place;
    var activity;
    var url;

    switch (this.props.major) {

      case 'Medieninformatik':
        company     = 'Imsimity';
        place       = 'St. Georgen im Schwarzwald';
        activity    = 'Tätigkeiten in der Softwareentwicklung, Bereich 3D Modelierung, HTML 5, Unity.'
        url         = 'http://imsimity.de/jobs.html';
        break;
      case 'IT-Sicherheit':
        company     = 'Campusjäger GmbH';
        place       = 'Karlsruhe';
        activity    = 'Entwicklung von Microservices mit Python.'
        url         = 'https://www.campusjaeger.de/jobs/praktikum-python-entwicklung-m-w-informatik-it-sicherheit-softwareentwicklung-karlsruhe';
        break;
      case 'Software Engineering':
        company     = 'Robert Bosch GmbH';
        place       = 'Abstatt';
        activity    = 'Tätigkeiten im Bereich Toolentwicklung';
        url         = 'https://www.praktikumsstellen.de/995010000004903410.html';
        break;
      case 'Allgemeine Informatik':
        company     = 'Drägerwerk AG & Co. KgaA';
        place       = 'Lübeck';
        activity    = 'Unterstützung bei der Schnittstellenentwicklung und Dokumentation für technische Beschreibungen und Assentverwaltung';
        url         = 'https://www.praktikumsstellen.de/995010000004903410.html';
        break;
    }

    return(
      <div>
        <h3>Praxissemester</h3>
        <br />
        <h5>Zum Beispiel bei <i>{company}</i></h5>
        <br />
        <h5>Aktivität</h5>
        <p>{activity}</p>
        <a href={url}></a>
      </div>
    );
  }
}

export default InternshipInfo;
