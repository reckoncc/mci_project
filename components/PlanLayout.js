import Layout from './Layout';
import Sidebar from './Sidebar';
import ModuleTable from './ModuleTable';''
import LectureInfo from './LectureInfo';
import {moduleInfo} from './data';

class PlanLayout extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      page: 'sem1'
    }

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();
    this.setState({
      page: e.target.id
    });
  }

  render() {
    return(
      <Layout>
        <div>
          <h1 className="display-4">{this.props.major}</h1>
          {this.props.children}
          <br />
          <div className="row">
            <div className="col-md-3">
              <Sidebar action={this.handleClick} />
            </div>
            <div className="col-md-7">
              <LectureInfo major={this.props.major} sem={this.state.page} />
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default PlanLayout;
