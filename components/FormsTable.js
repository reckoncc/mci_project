const FormsTable = (props) => (
  <table className="table">
    <thead>
      <tr>
        <th>{props.name}</th>
      </tr>
    </thead>
    <tbody>
      {
        props.data.map(item => {
          return (
            <tr>
              <td><a target="_blank" href={item.url}>{item.name}</a></td>
            </tr>
          );
        })
      }
    </tbody>
  </table>
);

export default FormsTable;
