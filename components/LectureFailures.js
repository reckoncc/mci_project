const LectureFailures = (props) => (
  <div>
    <h1 className="display-4">Vorlesungsausfälle</h1>
    {
      props.list.map(item =>
        <div>
          <h5><span className="badge badge-light">{item.date}</span> {item.course}</h5>
        </div>
      )
    }
  </div>
);

export default LectureFailures;
