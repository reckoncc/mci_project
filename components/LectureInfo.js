import informatik1  from '../static/informatik1.png';
import informatik2 from '../static/informatik2.png';
import informatik3 from '../static/informatik3.png';
import informatik4_IN from '../static/informatik4_IN.png';
import informatik4_IS from '../static/informatik4_IS.png';
import informatik4_MI from '../static/informatik4_MI.png';
import informatik4_SE from '../static/informatik4_SE.png';
import informatik67_IN from '../static/informatik67_IN.png';
import informatik67_IS from '../static/informatik67_IS.png';
import informatik67_MI from '../static/informatik67_MI.png';
import informatik67_SE from '../static/informatik67_SE.png';

import InternshipInfo from './InternshipInfo';

class LectureInfo extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    var img_url = '';
    var semester = '';

    switch (this.props.sem) {
      case 'sem1':
        img_url = informatik1;
        semester = 'Semester 1';
        break;
      case 'sem2':
        img_url = informatik2;
        semester = 'Semester 2';
        break;
      case 'sem3':
        img_url = informatik3;
        semester = 'Semester 3';
        break;
      case 'sem4':
        if (this.props.major === 'Allgemeine Informatik') {
          img_url = informatik4_IN;
        }
        else if (this.props.major === 'IT-Sicherheit') {
          img_url = informatik4_IS;
        }
        else if (this.props.major === 'Medieninformatik') {
          img_url = informatik4_MI;
        }
        else if (this.props.major === 'Software Engineering') {
          img_url = informatik4_SE;
        }
        semester = 'Semester 4';
        break;
      case 'sem5':
        img_url = '';
        break;
      case 'sem67':
        switch (this.props.major) {
          case 'Allgemeine Informatik':
            img_url = informatik67_IN;
            break;
          case 'IT-Sicherheit':
            img_url = informatik67_IS;
            break;
          case 'Medieninformatik':
            img_url = informatik67_MI;
            break;
          case 'Software Engineering':
            img_url = informatik67_SE;
        }
        semester = 'Semester 6/7';
        break;
      case 'wahl':
        semester = 'Wahlfächer';
    }

    return(
      <div>
        {(
          <h3>{semester}</h3>
        )}
        {(this.props.sem === 'sem5'
          ? <InternshipInfo major={this.props.major} />
          : <img width="800px" src={img_url} />
        )}

      </div>

    );
  }
}

export default LectureInfo;
