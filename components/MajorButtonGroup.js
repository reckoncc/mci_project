import MajorButton from './MajorButton';

const MajorButtonGroup  = (props) => (
  <div>
    <div className="row" >
      <MajorButton link="plan/SE" name="Software Engineering" width={props.width} height={props.height}/>
      <MajorButton link="plan/IS" name="IT-Sicherheit" width={props.width} height={props.height}/>
      <MajorButton link="plan/IN" name="Allgemeine Informatik" width={props.width} height={props.height}/>
      <MajorButton link="plan/MI" name="Medieninformatik" width={props.width} height={props.height}/>
    </div>
    </div>
);

export default MajorButtonGroup;
