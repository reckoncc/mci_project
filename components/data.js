export var news = [
  {
    'key': 1,
    'date': '2018-06-17',
    'header': 'Krawalle nach Deutschlandspiel',
    'content': 'Aufgrund des unglaublich vorhersehbaren Ausgangs des Deutschlandspiels gab es Krawalle am Campus Burren. Er bleibt deswegen für die nächsten beiden Tage geschlossen.'
  },
  {
    'key': 2,
    'date': '2018-06-14',
    'header': 'Ausbau der Parkplätze am Campus Burren',
    'content': "Nach gefühlt jahrhundertelangem Warten haben die verantwortlichen endlich auf die verzweifelten Schreie der Studenten nach mehr Parkplätzen reagiert. Nun wurde beschlossen das diese im kommenden Semester verdoppelt werden."
  }
];

export var lectureFailures = [
  {
    key: 1,
    date: '2018-06-12',
    course: 'Software Projekt Management (Verlegung auf Mittwoch 15:45; Raum 1.01)'
  }
];

export var moduleInfo = [
  {
    KNr: '57001',
    type: 'course',
    name: 'Grundlagen der Mathematik',
    cp: 5,
	extra_work: 'Tutorium, Vortest',
	type_of_test: 'Written Exam',
  },
  {
    KNr: '57002',
    type: 'module',
    name: 'Analysis',
    cp: 5,
	extra_work: 'Vortest',
	type_of_test: 'Written Exam',
  },
  {
    KNr: '57003',
    type: 'module',
    name: 'Rechnerarchitektur',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57004',
    type: 'module',
    name: 'Programmierung',
	cp: 10,
	extra_work: 'Testate, Vortest',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57104',
    type: 'course',
    name: 'Strukturierte Programmierung',
	cp: 5,
	extra_work: 'Testate',
    type_of_test: 'Written Exam',
  },
  //Medieninformatik erstes Semester
  {
    KNr: '57021',
    type: 'module',
    name: 'Techniken des Mediendesigns',
	cp: 5,
	extra_work: 'Film, SVG-Projekt',
    type_of_test: 'Written Exam, Projects',
  },
  //Semester 2
  //Teil von Programmierung
  {
    KNr: '57201',
    type: 'course',
    name: 'Objektorientierte Programmierung',
	cp: 5,
	extra_work: 'Vortest',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57006',
    type: 'module',
    name: 'Diskrete Mathematik und lineare Algebra',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57007',
    type: 'module',
    name: 'Wahrscheinlichkeitstheorie und Statistik',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57008',
    type: 'module',
    name: 'Algorithmen und Datenstrukturen 1',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  //Semester 3
  {
    KNr: '57010',
    type: 'module',
    name: 'Theoretische Informatik 1',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57011',
    type: 'module',
    name: 'Betriebssystheme',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57012',
    type: 'module',
    name: 'Algorithmen und Datenstrukturen 2',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57013',
    type: 'module',
    name: 'Objektorientierte Modellierung',
	cp: 5,
	extra_work: 'Praktikum',
    type_of_test: 'Written Exam',
  },
  //Semester 3 Allgemeine Informatik/Software Engineering
  {
    KNr: '57017',
    type: 'module',
    name: 'Programmierpraktikum',
	cp: 5,
	extra_work: 'Projekt',
    type_of_test: 'Projekt',
  },
   //Semester 3 IT-Sicherheit
  {
    KNr: '57018',
    type: 'module',
    name: 'Sichere Programmierung',
	cp: 5,
	extra_work: 'Projekte',
    type_of_test: 'Projekte',
  },

//Semester 4 alle
  {
    KNr: '57901',
    type: 'module',
    name: 'Software Engineering',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57903',
    type: 'module',
    name: 'Rechnernetze',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  //Semester 4 AGI
  {
    KNr: '57902',
    type: 'module',
    name: 'Software Project Management',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57904',
    type: 'module',
    name: 'Mensch-Computer-Interaktion',
	cp: 5,
	extra_work: 'Projekt',
    type_of_test: 'Written Exam',
  },
    //Semester 6 AGI
  {
    KNr: '57907',
    type: 'module',
    name: 'Compilerbau',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57908',
    type: 'module',
    name: 'Vortgeschrittene Programmierung',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
    //Semester 7 AGI
  {
    KNr: '57910',
    type: 'module',
    name: 'Cloud and Distributed Computing',
	cp: 5,
	extra_work: 'Project',
    type_of_test: 'Written Exam',
  },
  //Semester 4 ITS
  {
    KNr: '57915',
    type: 'module',
    name: 'Betriebswirtschaftslehre',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57917',
    type: 'module',
    name: 'Sichere Hardware',
	cp: 5,
	extra_work: 'Projekt',
    type_of_test: 'Projekt',
  },
  //Semester 6 ITS
  {
    KNr: '57919',
    type: 'module',
    name: 'Datenschutz',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57920',
    type: 'module',
    name: 'Netzwerksicherheit',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  //Semester 7 ITS
  {
    KNr: '57704',
    type: 'course',
    name: 'Kryptographische Protokolle',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57021',
    type: 'module',
    name: 'Systemsicherheit',
	cp: 5,
	extra_work: 'Projekt',
    type_of_test: 'Written Exam',
  },
  //Semester 4 MI
  {
    KNr: '57902',
    type: 'module',
    name: 'Software Project Management',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57925',
    type: 'module',
    name: 'Virtuelle Realität und Animation',
	cp: 5,
	extra_work: 'Projekte',
    type_of_test: 'Projekte',
  },
  //Semester 6 MI
  {
    KNr: '57907',
    type: 'module',
    name: 'Compilerbau',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57929',
    type: 'module',
    name: 'Bildverarbeitung und Musterekennung',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  //semester 7 MI
  {
    KNr: '57931',
    type: 'module',
    name: 'Computergraphik',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57932',
    type: 'module',
    name: 'Spieleprogrammierung',
	cp: 5,
	extra_work: 'Projekt',
    type_of_test: 'Projekt',
  },
  //Semester 4 SE
  {
    KNr: '57902',
    type: 'module',
    name: 'Software Project Management',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57936',
    type: 'module',
    name: 'Komponenntenbasierte Softwaretechnik',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  //Semester 6 SE
  {
    KNr: '57938',
    type: 'module',
    name: 'Mobile and Embedded Software Developement',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57939',
    type: 'module',
    name: 'Software Architecture',
	cp: 5,
	extra_work: '-',
    type_of_test: 'Written Exam',
  },
  //Semester 7 SE
  {
    KNr: '57910',
    type: 'module',
    name: 'Cloud and Distributed Computing',
	cp: 5,
	extra_work: 'Project',
    type_of_test: 'Written Exam',
  },
  {
    KNr: '57939',
    type: 'module',
    name: 'Software Quality',
	cp: 5,
	extra_work: 'Projekt',
    type_of_test: 'Projekt',
  },
];

export var formData = {
  arbeit: [
    {
      name: 'Kolloquium',
      url: 'https://www.hs-aalen.de/uploads/mediapool/media/file/301/Pruefarb_kolloquiumsbesuch_STG-Informatik.pdf',
    }
  ],
  praxis: [
    {
      name: 'Praxissemester Infoblatt',
      url: 'https://www.hs-aalen.de/de/courses/16/downloads',
    }
  ],
  pruefung: [
	{
      name: 'Prüfungsplan SoSe2018',
      url: 'https://www.hs-aalen.de/de/pages/pruefungsplan',
    }
  ],
  studiumGenerale: [
	{
      name: 'Informationen Studium Generale',
      url: 'https://www.hs-aalen.de/pages/career-center_faq',
    }
  ],
  zeugnis: [
	{
      name: 'Anmeldung zum QIS online Formular',
      url: 'https://qis-studenten.htw-aalen.de/qisserverstud/rds?state=user&type=0',
    }
  ],
  spo: [
	{
      name: 'Informationen zur SPO',
      url: 'https://www.hs-aalen.de/de/pages/studien-und-pruefungsordnungen-satzungen',
    }
  ],
  sonstiges: [
	{
      name: 'Semesterplan SoSe2018',
      url: 'https://www.hs-aalen.de/semesters/9',
    },
	{
      name: 'Semesterplan WS 2018/19',
      url: 'https://www.hs-aalen.de/semesters/10',
    },
	{
      name: 'Zentraler Studierendenservice',
      url: 'https://www.hs-aalen.de/de/facilities/8/downloads',
    }
  ]
};
