const News  = (props) => (
  <div>
    <h1 className="display-4">Aktuelles</h1>
    {
      props.list.map(item =>
        <div>
          <h5><span className="badge badge-light">{item.date}</span> {item.header}</h5>
          <p>{item.content}</p>
        </div>
      )
    }
  </div>
);

export default News;
