const Sidebar = (props) => (
  <div className="list-group">
     <a onClick={props.action} id="sem1" className="list-group-item list-group-item-action">Semester 1</a>
     <a onClick={props.action} id="sem2" className="list-group-item list-group-item-action">Semester 2</a>
     <a onClick={props.action} id="sem3" className="list-group-item list-group-item-action">Semester 3</a>
     <a onClick={props.action} id="sem4" className="list-group-item list-group-item-action">Semester 4</a>
     <a onClick={props.action} id="sem5" className="list-group-item list-group-item-action">Praxissemester</a>
     <a onClick={props.action} id="sem67" className="list-group-item list-group-item-action">Semester 6/7</a>
     <a onClick={props.action} id="wahl" className="list-group-item list-group-item-action">Wahlfächer</a>
   </div>
);

export default Sidebar;
