import Link from 'next/link';

const MajorButton = (props) => {
  return (
    <Link href={props.link}>
      <button
        type="button"
        className="btn btn-outline-info btn"
        style={{width: props.width + 'px', height: props.height + 'px', fontSize: '28px'}}
        >
        {props.name}
      </button>
    </Link>
  );
}

export default MajorButton;
