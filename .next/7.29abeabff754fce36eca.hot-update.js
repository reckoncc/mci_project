webpackHotUpdate(7,{

/***/ "./components/InternshipInfo.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/InternshipInfo.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InternshipInfo =
/*#__PURE__*/
function (_React$Component) {
  _inherits(InternshipInfo, _React$Component);

  function InternshipInfo(props) {
    _classCallCheck(this, InternshipInfo);

    return _possibleConstructorReturn(this, (InternshipInfo.__proto__ || Object.getPrototypeOf(InternshipInfo)).call(this, props));
  }

  _createClass(InternshipInfo, [{
    key: "render",
    value: function render() {
      var company;
      var place;
      var activity;
      var url;

      switch (this.props.major) {
        case 'Medieninformatik':
          company = 'Imsimity';
          place = 'St. Georgen im Schwarzwald';
          activity = 'Tätigkeiten in der Softwareentwicklung, Bereich 3D Modelierung, HTML 5, Unity.';
          url = 'http://imsimity.de/jobs.html';
          break;

        case 'IT-Sicherheit':
          company = 'Campusjäger GmbH';
          place = 'Karlsruhe';
          activity = 'Entwicklung von Microservices mit Python.';
          url = 'https://www.campusjaeger.de/jobs/praktikum-python-entwicklung-m-w-informatik-it-sicherheit-softwareentwicklung-karlsruhe';
          break;

        case 'Software Engineering':
          company = 'Robert Bosch GmbH';
          place = 'Abstatt';
          activity = 'Tätigkeiten im Bereich Toolentwicklung';
          url = 'https://www.praktikumsstellen.de/995010000004903410.html';
          break;

        case 'Allgemeine Informatik':
          company = 'Drägerwerk AG & Co. KgaA';
          place = 'Lübeck';
          activity = 'Unterstützung bei der Schnittstellenentwicklung und Dokumentation für technische Beschreibungen und Assentverwaltung';
          url = 'https://www.praktikumsstellen.de/995010000004903410.html';
          break;
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        }
      }, "Praxissemester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h5", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        }
      }, "Zum Beispiel bei ", __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("i", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        }
      }, company)), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h5", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        }
      }, "Aktivit\xE4t"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        }
      }, activity), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        href: url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        }
      }));
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);

  return InternshipInfo;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var _default = InternshipInfo;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(InternshipInfo, "InternshipInfo", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/InternshipInfo.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/InternshipInfo.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=7.29abeabff754fce36eca.hot-update.js.map