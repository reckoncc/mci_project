module.exports =

        __NEXT_REGISTER_PAGE('/plan/SE', function() {
          var comp = 
      webpackJsonp([7],{

/***/ "./components/InternshipInfo.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/InternshipInfo.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InternshipInfo =
/*#__PURE__*/
function (_React$Component) {
  _inherits(InternshipInfo, _React$Component);

  function InternshipInfo(props) {
    _classCallCheck(this, InternshipInfo);

    return _possibleConstructorReturn(this, (InternshipInfo.__proto__ || Object.getPrototypeOf(InternshipInfo)).call(this, props));
  }

  _createClass(InternshipInfo, [{
    key: "render",
    value: function render() {
      var company;
      var place;
      var activity;
      var url;

      switch (this.props.major) {
        case 'Medieninformatik':
          company = 'Imsimity';
          place = 'St. Georgen im Schwarzwald';
          activity = 'Tätigkeiten in der Softwareentwicklung, Bereich 3D Modelierung, HTML 5, Unity.';
          url = 'http://imsimity.de/jobs.html';
          break;

        case 'IT-Sicherheit':
          company = 'Campusjäger GmbH';
          place = 'Karlsruhe';
          activity = 'Entwicklung von Microservices mit Python.';
          url = 'https://www.campusjaeger.de/jobs/praktikum-python-entwicklung-m-w-informatik-it-sicherheit-softwareentwicklung-karlsruhe';
          break;

        case 'Software Engineering':
          company = 'Robert Bosch GmbH';
          place = 'Abstatt';
          activity = 'Tätigkeiten im Bereich Toolentwicklung';
          url = 'https://www.praktikumsstellen.de/995010000004903410.html';
          break;

        case 'Allgemeine Informatik':
          company = 'Drägerwerk AG & Co. KgaA';
          place = 'Lübeck';
          activity = 'Unterstützung bei der Schnittstellenentwicklung und Dokumentation für technische Beschreibungen und Assentverwaltung';
          url = 'https://www.praktikumsstellen.de/995010000004903410.html';
          break;
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        }
      }, "Praxissemester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h5", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        }
      }, "Zum Beispiel bei ", __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("i", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        }
      }, company)), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h5", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        }
      }, "Aktivit\xE4t"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        }
      }, activity), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        href: url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        }
      }));
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);

  return InternshipInfo;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var _default = InternshipInfo;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(InternshipInfo, "InternshipInfo", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/InternshipInfo.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/InternshipInfo.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./components/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_head__ = __webpack_require__("./node_modules/next/head.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_head___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_head__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Navbar__ = __webpack_require__("./components/Navbar.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Layout.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();




var Layout = function Layout(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_head___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Informatik HS Aalen"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("link", {
    rel: "stylesheet",
    href: "https://bootswatch.com/4/simplex/bootstrap.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__Navbar__["a" /* default */], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    }
  }, props.children));
};

var _default = Layout;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Layout, "Layout", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Layout.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Layout.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./components/LectureInfo.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__static_informatik1_png__ = __webpack_require__("./static/informatik1.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__static_informatik1_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__static_informatik1_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__static_informatik2_png__ = __webpack_require__("./static/informatik2.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__static_informatik2_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__static_informatik2_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__static_informatik3_png__ = __webpack_require__("./static/informatik3.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__static_informatik3_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__static_informatik3_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png__ = __webpack_require__("./static/informatik4_IN.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png__ = __webpack_require__("./static/informatik4_IS.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png__ = __webpack_require__("./static/informatik4_MI.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png__ = __webpack_require__("./static/informatik4_SE.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png__ = __webpack_require__("./static/informatik67_IN.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png__ = __webpack_require__("./static/informatik67_IS.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png__ = __webpack_require__("./static/informatik67_MI.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png__ = __webpack_require__("./static/informatik67_SE.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__InternshipInfo__ = __webpack_require__("./components/InternshipInfo.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/LectureInfo.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }














var LectureInfo =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LectureInfo, _React$Component);

  function LectureInfo(props) {
    _classCallCheck(this, LectureInfo);

    return _possibleConstructorReturn(this, (LectureInfo.__proto__ || Object.getPrototypeOf(LectureInfo)).call(this, props));
  }

  _createClass(LectureInfo, [{
    key: "render",
    value: function render() {
      var img_url = '';
      var semester = '';

      switch (this.props.sem) {
        case 'sem1':
          img_url = __WEBPACK_IMPORTED_MODULE_1__static_informatik1_png___default.a;
          semester = 'Semester 1';
          break;

        case 'sem2':
          img_url = __WEBPACK_IMPORTED_MODULE_2__static_informatik2_png___default.a;
          semester = 'Semester 2';
          break;

        case 'sem3':
          img_url = __WEBPACK_IMPORTED_MODULE_3__static_informatik3_png___default.a;
          semester = 'Semester 3';
          break;

        case 'sem4':
          if (this.props.major === 'Allgemeine Informatik') {
            img_url = __WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png___default.a;
          } else if (this.props.major === 'IT-Sicherheit') {
            img_url = __WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png___default.a;
          } else if (this.props.major === 'Medieninformatik') {
            img_url = __WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png___default.a;
          } else if (this.props.major === 'Software Engineering') {
            img_url = __WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png___default.a;
          }

          semester = 'Semester 4';
          break;

        case 'sem5':
          img_url = '';
          break;

        case 'sem67':
          switch (this.props.major) {
            case 'Allgemeine Informatik':
              img_url = __WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png___default.a;
              break;

            case 'IT-Sicherheit':
              img_url = __WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png___default.a;
              break;

            case 'Medieninformatik':
              img_url = __WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png___default.a;
              break;

            case 'Software Engineering':
              img_url = __WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png___default.a;
          }

          semester = 'Semester 6/7';
          break;

        case 'wahl':
          semester = 'Wahlfächer';
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        }
      }, semester), this.props.sem === 'sem5' ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_12__InternshipInfo__["a" /* default */], {
        major: this.props.major,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        }
      }) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("img", {
        width: "800px",
        src: img_url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        }
      }));
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);

  return LectureInfo;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var _default = LectureInfo;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(LectureInfo, "LectureInfo", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/LectureInfo.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/LectureInfo.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./components/ModuleTable.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/ModuleTable.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

var ModuleTable = function ModuleTable(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 2
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("table", {
    className: "table",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("thead", {
    className: "thead-dark",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, "KNr"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Kurs / Modul"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, "Semester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, "CPs"))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tbody", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    }
  }, props.data.map(function (item) {
    if (item.type === 'module') {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
        className: "table-secondary",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 17
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        target: "_blank",
        href: item.url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        }
      }, item.KNr)), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        colSpan: "3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19
        }
      }, item.name));
    } else if (item.type === 'course') {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 24
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 25
        }
      }, item.KNr), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26
        }
      }, item.name), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 27
        }
      }, item.semester), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        }
      }, item.cp));
    }
  }))));
};

var _default = ModuleTable;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(ModuleTable, "ModuleTable", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/ModuleTable.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/ModuleTable.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./components/Navbar.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link__ = __webpack_require__("./node_modules/next/link.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_link__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Navbar.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();



var Navbar = function Navbar() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("nav", {
    className: "navbar navbar-expand navbar-dark bg-dark mb-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "navbar-brand",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Info"), "rmatik")), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "collapse navbar-collapse",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("ul", {
    className: "navbar-nav ml-auto",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("li", {
    className: "nav-item",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/modulbeschreibung",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "nav-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    }
  }, "Modulbeschreibung"))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("li", {
    className: "nav-item",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/formulare",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "nav-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    }
  }, "Formulare")))))));
};

var _default = Navbar;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Navbar, "Navbar", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Navbar.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Navbar.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./components/PlanLayout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Layout__ = __webpack_require__("./components/Layout.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Sidebar__ = __webpack_require__("./components/Sidebar.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ModuleTable__ = __webpack_require__("./components/ModuleTable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__LectureInfo__ = __webpack_require__("./components/LectureInfo.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data__ = __webpack_require__("./components/data.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/PlanLayout.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }




'';



var PlanLayout =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PlanLayout, _React$Component);

  function PlanLayout(props) {
    var _this;

    _classCallCheck(this, PlanLayout);

    _this = _possibleConstructorReturn(this, (PlanLayout.__proto__ || Object.getPrototypeOf(PlanLayout)).call(this, props));
    _this.state = {
      page: 'sem1'
    };
    _this.handleClick = _this.handleClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(PlanLayout, [{
    key: "handleClick",
    value: function handleClick(e) {
      e.preventDefault();
      this.setState({
        page: e.target.id
      });
    }
  }, {
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__Layout__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h1", {
        className: "display-4",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        }
      }, this.props.major), this.props.children, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "row",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "col-md-3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__Sidebar__["a" /* default */], {
        action: this.handleClick,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        }
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "col-md-7",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__LectureInfo__["a" /* default */], {
        major: this.props.major,
        sem: this.state.page,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        }
      })))));
    }
  }, {
    key: "__reactstandin__regenerateByEval",
    // @ts-ignore
    value: function __reactstandin__regenerateByEval(key, code) {
      // @ts-ignore
      this[key] = eval(code);
    }
  }]);

  return PlanLayout;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

var _default = PlanLayout;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(PlanLayout, "PlanLayout", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/PlanLayout.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/PlanLayout.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./components/Sidebar.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Sidebar.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

var Sidebar = function Sidebar(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "list-group",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 2
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem1",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    }
  }, "Semester 1"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem2",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, "Semester 2"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem3",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, "Semester 3"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem4",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, "Semester 4"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem5",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Praxissemester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem67",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, "Semester 6/7"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "wahl",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, "Wahlf\xE4cher"));
};

var _default = Sidebar;
/* harmony default export */ __webpack_exports__["a"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(Sidebar, "Sidebar", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Sidebar.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Sidebar.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./components/data.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(module) {/* unused harmony export news */
/* unused harmony export lectureFailures */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return moduleInfo; });
/* unused harmony export formData */
(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();

var news = [{
  'key': 1,
  'date': '2018-06-17',
  'header': 'Krawalle nach Deutschlandspiel',
  'content': 'Aufgrund des unglaublich vorhersehbaren Ausgangs des Deutschlandspiels gab es Krawalle am Campus Burren. Er bleibt deswegen für die nächsten beiden Tage geschlossen.'
}, {
  'key': 2,
  'date': '2018-06-14',
  'header': 'Ausbau der Parkplätze am Campus Burren',
  'content': "Nach gefühlt jahrhundertelangem Warten haben die verantwortlichen endlich auf die verzweifelten Schreie der Studenten nach mehr Parkplätzen reagiert. Nun wurde beschlossen das diese im kommenden Semester verdoppelt werden."
}];
var lectureFailures = [{
  key: 1,
  date: '2018-06-12',
  course: 'Software Projekt Management (Verlegung auf Mittwoch 15:45; Raum 1.01)'
}];
var moduleInfo = [{
  KNr: '57001',
  type: 'course',
  name: 'Grundlagen der Mathematik',
  cp: 5,
  extra_work: 'Tutorium, Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57002',
  type: 'module',
  name: 'Analysis',
  cp: 5,
  extra_work: 'Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57003',
  type: 'module',
  name: 'Rechnerarchitektur',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57004',
  type: 'module',
  name: 'Programmierung',
  cp: 10,
  extra_work: 'Testate, Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57104',
  type: 'course',
  name: 'Strukturierte Programmierung',
  cp: 5,
  extra_work: 'Testate',
  type_of_test: 'Written Exam'
}, //Medieninformatik erstes Semester
{
  KNr: '57021',
  type: 'module',
  name: 'Techniken des Mediendesigns',
  cp: 5,
  extra_work: 'Film, SVG-Projekt',
  type_of_test: 'Written Exam, Projects'
}, //Semester 2
//Teil von Programmierung
{
  KNr: '57201',
  type: 'course',
  name: 'Objektorientierte Programmierung',
  cp: 5,
  extra_work: 'Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57006',
  type: 'module',
  name: 'Diskrete Mathematik und lineare Algebra',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57007',
  type: 'module',
  name: 'Wahrscheinlichkeitstheorie und Statistik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57008',
  type: 'module',
  name: 'Algorithmen und Datenstrukturen 1',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 3
{
  KNr: '57010',
  type: 'module',
  name: 'Theoretische Informatik 1',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57011',
  type: 'module',
  name: 'Betriebssystheme',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57012',
  type: 'module',
  name: 'Algorithmen und Datenstrukturen 2',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57013',
  type: 'module',
  name: 'Objektorientierte Modellierung',
  cp: 5,
  extra_work: 'Praktikum',
  type_of_test: 'Written Exam'
}, //Semester 3 Allgemeine Informatik/Software Engineering
{
  KNr: '57017',
  type: 'module',
  name: 'Programmierpraktikum',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 3 IT-Sicherheit
{
  KNr: '57018',
  type: 'module',
  name: 'Sichere Programmierung',
  cp: 5,
  extra_work: 'Projekte',
  type_of_test: 'Projekte'
}, //Semester 4 alle
{
  KNr: '57901',
  type: 'module',
  name: 'Software Engineering',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57903',
  type: 'module',
  name: 'Rechnernetze',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 4 AGI
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57904',
  type: 'module',
  name: 'Mensch-Computer-Interaktion',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Written Exam'
}, //Semester 6 AGI
{
  KNr: '57907',
  type: 'module',
  name: 'Compilerbau',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57908',
  type: 'module',
  name: 'Vortgeschrittene Programmierung',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 AGI
{
  KNr: '57910',
  type: 'module',
  name: 'Cloud and Distributed Computing',
  cp: 5,
  extra_work: 'Project',
  type_of_test: 'Written Exam'
}, //Semester 4 ITS
{
  KNr: '57915',
  type: 'module',
  name: 'Betriebswirtschaftslehre',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57917',
  type: 'module',
  name: 'Sichere Hardware',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 6 ITS
{
  KNr: '57919',
  type: 'module',
  name: 'Datenschutz',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57920',
  type: 'module',
  name: 'Netzwerksicherheit',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 ITS
{
  KNr: '57704',
  type: 'course',
  name: 'Kryptographische Protokolle',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57021',
  type: 'module',
  name: 'Systemsicherheit',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Written Exam'
}, //Semester 4 MI
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57925',
  type: 'module',
  name: 'Virtuelle Realität und Animation',
  cp: 5,
  extra_work: 'Projekte',
  type_of_test: 'Projekte'
}, //Semester 6 MI
{
  KNr: '57907',
  type: 'module',
  name: 'Compilerbau',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57929',
  type: 'module',
  name: 'Bildverarbeitung und Musterekennung',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //semester 7 MI
{
  KNr: '57931',
  type: 'module',
  name: 'Computergraphik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57932',
  type: 'module',
  name: 'Spieleprogrammierung',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 4 SE
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57936',
  type: 'module',
  name: 'Komponenntenbasierte Softwaretechnik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 6 SE
{
  KNr: '57938',
  type: 'module',
  name: 'Mobile and Embedded Software Developement',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57939',
  type: 'module',
  name: 'Software Architecture',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 SE
{
  KNr: '57910',
  type: 'module',
  name: 'Cloud and Distributed Computing',
  cp: 5,
  extra_work: 'Project',
  type_of_test: 'Written Exam'
}, {
  KNr: '57939',
  type: 'module',
  name: 'Software Quality',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}];
var formData = {
  arbeit: [{
    name: 'Kolloquium',
    url: 'https://www.hs-aalen.de/uploads/mediapool/media/file/301/Pruefarb_kolloquiumsbesuch_STG-Informatik.pdf'
  }],
  praxis: [{
    name: 'Praxissemester Infoblatt',
    url: 'https://www.hs-aalen.de/de/courses/16/downloads'
  }],
  pruefung: [{
    name: 'Prüfungsplan SoSe2018',
    url: 'https://www.hs-aalen.de/de/pages/pruefungsplan'
  }],
  studiumGenerale: [{
    name: 'Informationen Studium Generale',
    url: 'https://www.hs-aalen.de/pages/career-center_faq'
  }],
  zeugnis: [{
    name: 'Anmeldung zum QIS online Formular',
    url: 'https://qis-studenten.htw-aalen.de/qisserverstud/rds?state=user&type=0'
  }],
  spo: [{
    name: 'Informationen zur SPO',
    url: 'https://www.hs-aalen.de/de/pages/studien-und-pruefungsordnungen-satzungen'
  }],
  sonstiges: [{
    name: 'Semesterplan SoSe2018',
    url: 'https://www.hs-aalen.de/semesters/9'
  }, {
    name: 'Semesterplan WS 2018/19',
    url: 'https://www.hs-aalen.de/semesters/10'
  }, {
    name: 'Zentraler Studierendenservice',
    url: 'https://www.hs-aalen.de/de/facilities/8/downloads'
  }]
};
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(news, "news", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/data.js");
  reactHotLoader.register(lectureFailures, "lectureFailures", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/data.js");
  reactHotLoader.register(moduleInfo, "moduleInfo", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/data.js");
  reactHotLoader.register(formData, "formData", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/data.js");
  leaveModule(module);
})();

;
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./node_modules/@babel/runtime/core-js/json/stringify.js":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./node_modules/core-js/library/fn/json/stringify.js");

/***/ }),

/***/ "./node_modules/core-js/library/fn/json/stringify.js":
/***/ (function(module, exports, __webpack_require__) {

var core = __webpack_require__("./node_modules/core-js/library/modules/_core.js");
var $JSON = core.JSON || (core.JSON = { stringify: JSON.stringify });
module.exports = function stringify(it) { // eslint-disable-line no-unused-vars
  return $JSON.stringify.apply($JSON, arguments);
};


/***/ }),

/***/ "./node_modules/define-properties/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var keys = __webpack_require__("./node_modules/object-keys/index.js");
var foreach = __webpack_require__("./node_modules/foreach/index.js");
var hasSymbols = typeof Symbol === 'function' && typeof Symbol() === 'symbol';

var toStr = Object.prototype.toString;

var isFunction = function (fn) {
	return typeof fn === 'function' && toStr.call(fn) === '[object Function]';
};

var arePropertyDescriptorsSupported = function () {
	var obj = {};
	try {
		Object.defineProperty(obj, 'x', { enumerable: false, value: obj });
        /* eslint-disable no-unused-vars, no-restricted-syntax */
        for (var _ in obj) { return false; }
        /* eslint-enable no-unused-vars, no-restricted-syntax */
		return obj.x === obj;
	} catch (e) { /* this is IE 8. */
		return false;
	}
};
var supportsDescriptors = Object.defineProperty && arePropertyDescriptorsSupported();

var defineProperty = function (object, name, value, predicate) {
	if (name in object && (!isFunction(predicate) || !predicate())) {
		return;
	}
	if (supportsDescriptors) {
		Object.defineProperty(object, name, {
			configurable: true,
			enumerable: false,
			value: value,
			writable: true
		});
	} else {
		object[name] = value;
	}
};

var defineProperties = function (object, map) {
	var predicates = arguments.length > 2 ? arguments[2] : {};
	var props = keys(map);
	if (hasSymbols) {
		props = props.concat(Object.getOwnPropertySymbols(map));
	}
	foreach(props, function (name) {
		defineProperty(object, name, map[name], predicates[name]);
	});
};

defineProperties.supportsDescriptors = !!supportsDescriptors;

module.exports = defineProperties;


/***/ }),

/***/ "./node_modules/foreach/index.js":
/***/ (function(module, exports) {


var hasOwn = Object.prototype.hasOwnProperty;
var toString = Object.prototype.toString;

module.exports = function forEach (obj, fn, ctx) {
    if (toString.call(fn) !== '[object Function]') {
        throw new TypeError('iterator must be a function');
    }
    var l = obj.length;
    if (l === +l) {
        for (var i = 0; i < l; i++) {
            fn.call(ctx, obj[i], i, obj);
        }
    } else {
        for (var k in obj) {
            if (hasOwn.call(obj, k)) {
                fn.call(ctx, obj[k], k, obj);
            }
        }
    }
};



/***/ }),

/***/ "./node_modules/function-bind/implementation.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint no-invalid-this: 1 */

var ERROR_MESSAGE = 'Function.prototype.bind called on incompatible ';
var slice = Array.prototype.slice;
var toStr = Object.prototype.toString;
var funcType = '[object Function]';

module.exports = function bind(that) {
    var target = this;
    if (typeof target !== 'function' || toStr.call(target) !== funcType) {
        throw new TypeError(ERROR_MESSAGE + target);
    }
    var args = slice.call(arguments, 1);

    var bound;
    var binder = function () {
        if (this instanceof bound) {
            var result = target.apply(
                this,
                args.concat(slice.call(arguments))
            );
            if (Object(result) === result) {
                return result;
            }
            return this;
        } else {
            return target.apply(
                that,
                args.concat(slice.call(arguments))
            );
        }
    };

    var boundLength = Math.max(0, target.length - args.length);
    var boundArgs = [];
    for (var i = 0; i < boundLength; i++) {
        boundArgs.push('$' + i);
    }

    bound = Function('binder', 'return function (' + boundArgs.join(',') + '){ return binder.apply(this,arguments); }')(binder);

    if (target.prototype) {
        var Empty = function Empty() {};
        Empty.prototype = target.prototype;
        bound.prototype = new Empty();
        Empty.prototype = null;
    }

    return bound;
};


/***/ }),

/***/ "./node_modules/function-bind/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__("./node_modules/function-bind/implementation.js");

module.exports = Function.prototype.bind || implementation;


/***/ }),

/***/ "./node_modules/has-symbols/shams.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/* eslint complexity: [2, 17], max-statements: [2, 33] */
module.exports = function hasSymbols() {
	if (typeof Symbol !== 'function' || typeof Object.getOwnPropertySymbols !== 'function') { return false; }
	if (typeof Symbol.iterator === 'symbol') { return true; }

	var obj = {};
	var sym = Symbol('test');
	var symObj = Object(sym);
	if (typeof sym === 'string') { return false; }

	if (Object.prototype.toString.call(sym) !== '[object Symbol]') { return false; }
	if (Object.prototype.toString.call(symObj) !== '[object Symbol]') { return false; }

	// temp disabled per https://github.com/ljharb/object.assign/issues/17
	// if (sym instanceof Symbol) { return false; }
	// temp disabled per https://github.com/WebReflection/get-own-property-symbols/issues/4
	// if (!(symObj instanceof Symbol)) { return false; }

	// if (typeof Symbol.prototype.toString !== 'function') { return false; }
	// if (String(sym) !== Symbol.prototype.toString.call(sym)) { return false; }

	var symVal = 42;
	obj[sym] = symVal;
	for (sym in obj) { return false; } // eslint-disable-line no-restricted-syntax
	if (typeof Object.keys === 'function' && Object.keys(obj).length !== 0) { return false; }

	if (typeof Object.getOwnPropertyNames === 'function' && Object.getOwnPropertyNames(obj).length !== 0) { return false; }

	var syms = Object.getOwnPropertySymbols(obj);
	if (syms.length !== 1 || syms[0] !== sym) { return false; }

	if (!Object.prototype.propertyIsEnumerable.call(obj, sym)) { return false; }

	if (typeof Object.getOwnPropertyDescriptor === 'function') {
		var descriptor = Object.getOwnPropertyDescriptor(obj, sym);
		if (descriptor.value !== symVal || descriptor.enumerable !== true) { return false; }
	}

	return true;
};


/***/ }),

/***/ "./node_modules/has/src/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var bind = __webpack_require__("./node_modules/function-bind/index.js");

module.exports = bind.call(Function.call, Object.prototype.hasOwnProperty);


/***/ }),

/***/ "./node_modules/next/dist/lib/link.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _interopRequireWildcard = __webpack_require__("./node_modules/@babel/runtime/helpers/interopRequireWildcard.js");

var _interopRequireDefault = __webpack_require__("./node_modules/@babel/runtime/helpers/interopRequireDefault.js");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _typeof2 = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/helpers/typeof.js"));

var _stringify = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/core-js/json/stringify.js"));

var _getPrototypeOf = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/core-js/object/get-prototype-of.js"));

var _classCallCheck2 = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/helpers/classCallCheck.js"));

var _createClass2 = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/helpers/createClass.js"));

var _possibleConstructorReturn2 = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/helpers/possibleConstructorReturn.js"));

var _inherits2 = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/helpers/inherits.js"));

var _assertThisInitialized2 = _interopRequireDefault(__webpack_require__("./node_modules/@babel/runtime/helpers/assertThisInitialized.js"));

var _url = __webpack_require__("./node_modules/url/url.js");

var _react = _interopRequireWildcard(__webpack_require__("./node_modules/react/cjs/react.development.js"));

var _propTypes = _interopRequireDefault(__webpack_require__("./node_modules/prop-types/index.js"));

var _propTypesExact = _interopRequireDefault(__webpack_require__("./node_modules/prop-types-exact/build/index.js"));

var _router = _interopRequireWildcard(__webpack_require__("./node_modules/next/dist/lib/router/index.js"));

var _utils = __webpack_require__("./node_modules/next/dist/lib/utils.js");

/* global __NEXT_DATA__ */
var Link =
/*#__PURE__*/
function (_Component) {
  (0, _inherits2.default)(Link, _Component);

  function Link(props) {
    var _ref;

    var _this;

    (0, _classCallCheck2.default)(this, Link);

    for (var _len = arguments.length, rest = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      rest[_key - 1] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2.default)(this, (_ref = Link.__proto__ || (0, _getPrototypeOf.default)(Link)).call.apply(_ref, [this, props].concat(rest)));
    _this.linkClicked = _this.linkClicked.bind((0, _assertThisInitialized2.default)(_this));

    _this.formatUrls(props);

    return _this;
  }

  (0, _createClass2.default)(Link, [{
    key: "componentWillReceiveProps",
    value: function componentWillReceiveProps(nextProps) {
      this.formatUrls(nextProps);
    }
  }, {
    key: "linkClicked",
    value: function linkClicked(e) {
      var _this2 = this;

      if (e.currentTarget.nodeName === 'A' && (e.metaKey || e.ctrlKey || e.shiftKey || e.nativeEvent && e.nativeEvent.which === 2)) {
        // ignore click for new tab / new window behavior
        return;
      }

      var shallow = this.props.shallow;
      var href = this.href,
          as = this.as;

      if (!isLocal(href)) {
        // ignore click if it's outside our scope
        return;
      }

      var pathname = window.location.pathname;
      href = (0, _url.resolve)(pathname, href);
      as = as ? (0, _url.resolve)(pathname, as) : href;
      e.preventDefault(); //  avoid scroll for urls with anchor refs

      var scroll = this.props.scroll;

      if (scroll == null) {
        scroll = as.indexOf('#') < 0;
      } // replace state instead of push if prop is present


      var replace = this.props.replace;
      var changeMethod = replace ? 'replace' : 'push'; // straight up redirect

      _router.default[changeMethod](href, as, {
        shallow: shallow
      }).then(function (success) {
        if (!success) return;

        if (scroll) {
          window.scrollTo(0, 0);
          document.body.focus();
        }
      }).catch(function (err) {
        if (_this2.props.onError) _this2.props.onError(err);
      });
    }
  }, {
    key: "prefetch",
    value: function prefetch() {
      if (!this.props.prefetch) return;
      if (typeof window === 'undefined') return; // Prefetch the JSON page if asked (only in the client)

      var pathname = window.location.pathname;
      var href = (0, _url.resolve)(pathname, this.href);

      _router.default.prefetch(href);
    }
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      this.prefetch();
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps) {
      if ((0, _stringify.default)(this.props.href) !== (0, _stringify.default)(prevProps.href)) {
        this.prefetch();
      }
    } // We accept both 'href' and 'as' as objects which we can pass to `url.format`.
    // We'll handle it here.

  }, {
    key: "formatUrls",
    value: function formatUrls(props) {
      this.href = props.href && (0, _typeof2.default)(props.href) === 'object' ? (0, _url.format)(props.href) : props.href;
      this.as = props.as && (0, _typeof2.default)(props.as) === 'object' ? (0, _url.format)(props.as) : props.as;
    }
  }, {
    key: "render",
    value: function render() {
      var children = this.props.children;
      var href = this.href,
          as = this.as; // Deprecated. Warning shown by propType check. If the childen provided is a string (<Link>example</Link>) we wrap it in an <a> tag

      if (typeof children === 'string') {
        children = _react.default.createElement("a", null, children);
      } // This will return the first child, if multiple are provided it will throw an error


      var child = _react.Children.only(children);

      var props = {
        onClick: this.linkClicked // If child is an <a> tag and doesn't have a href attribute, or if the 'passHref' property is
        // defined, we specify the current 'href', so that repetition is not needed by the user

      };

      if (this.props.passHref || child.type === 'a' && !('href' in child.props)) {
        props.href = as || href;
      } // Add the ending slash to the paths. So, we can serve the
      // "<page>/index.html" directly.


      if (props.href && typeof __NEXT_DATA__ !== 'undefined' && __NEXT_DATA__.nextExport) {
        props.href = (0, _router._rewriteUrlForNextExport)(props.href);
      }

      return _react.default.cloneElement(child, props);
    }
  }]);
  return Link;
}(_react.Component);

exports.default = Link;
Object.defineProperty(Link, "propTypes", {
  configurable: true,
  enumerable: true,
  writable: true,
  value: (0, _propTypesExact.default)({
    href: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]).isRequired,
    as: _propTypes.default.oneOfType([_propTypes.default.string, _propTypes.default.object]),
    prefetch: _propTypes.default.bool,
    replace: _propTypes.default.bool,
    shallow: _propTypes.default.bool,
    passHref: _propTypes.default.bool,
    scroll: _propTypes.default.bool,
    children: _propTypes.default.oneOfType([_propTypes.default.element, function (props, propName) {
      var value = props[propName];

      if (typeof value === 'string') {
        warnLink("Warning: You're using a string directly inside <Link>. This usage has been deprecated. Please add an <a> tag as child of <Link>");
      }

      return null;
    }]).isRequired
  })
});

function isLocal(href) {
  var url = (0, _url.parse)(href, false, true);
  var origin = (0, _url.parse)((0, _utils.getLocationOrigin)(), false, true);
  return !url.host || url.protocol === origin.protocol && url.host === origin.host;
}

var warnLink = (0, _utils.execOnce)(_utils.warn);

/***/ }),

/***/ "./node_modules/next/head.js":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./node_modules/next/dist/lib/head.js")


/***/ }),

/***/ "./node_modules/next/link.js":
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./node_modules/next/dist/lib/link.js")


/***/ }),

/***/ "./node_modules/object-keys/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// modified from https://github.com/es-shims/es5-shim
var has = Object.prototype.hasOwnProperty;
var toStr = Object.prototype.toString;
var slice = Array.prototype.slice;
var isArgs = __webpack_require__("./node_modules/object-keys/isArguments.js");
var isEnumerable = Object.prototype.propertyIsEnumerable;
var hasDontEnumBug = !isEnumerable.call({ toString: null }, 'toString');
var hasProtoEnumBug = isEnumerable.call(function () {}, 'prototype');
var dontEnums = [
	'toString',
	'toLocaleString',
	'valueOf',
	'hasOwnProperty',
	'isPrototypeOf',
	'propertyIsEnumerable',
	'constructor'
];
var equalsConstructorPrototype = function (o) {
	var ctor = o.constructor;
	return ctor && ctor.prototype === o;
};
var excludedKeys = {
	$console: true,
	$external: true,
	$frame: true,
	$frameElement: true,
	$frames: true,
	$innerHeight: true,
	$innerWidth: true,
	$outerHeight: true,
	$outerWidth: true,
	$pageXOffset: true,
	$pageYOffset: true,
	$parent: true,
	$scrollLeft: true,
	$scrollTop: true,
	$scrollX: true,
	$scrollY: true,
	$self: true,
	$webkitIndexedDB: true,
	$webkitStorageInfo: true,
	$window: true
};
var hasAutomationEqualityBug = (function () {
	/* global window */
	if (typeof window === 'undefined') { return false; }
	for (var k in window) {
		try {
			if (!excludedKeys['$' + k] && has.call(window, k) && window[k] !== null && typeof window[k] === 'object') {
				try {
					equalsConstructorPrototype(window[k]);
				} catch (e) {
					return true;
				}
			}
		} catch (e) {
			return true;
		}
	}
	return false;
}());
var equalsConstructorPrototypeIfNotBuggy = function (o) {
	/* global window */
	if (typeof window === 'undefined' || !hasAutomationEqualityBug) {
		return equalsConstructorPrototype(o);
	}
	try {
		return equalsConstructorPrototype(o);
	} catch (e) {
		return false;
	}
};

var keysShim = function keys(object) {
	var isObject = object !== null && typeof object === 'object';
	var isFunction = toStr.call(object) === '[object Function]';
	var isArguments = isArgs(object);
	var isString = isObject && toStr.call(object) === '[object String]';
	var theKeys = [];

	if (!isObject && !isFunction && !isArguments) {
		throw new TypeError('Object.keys called on a non-object');
	}

	var skipProto = hasProtoEnumBug && isFunction;
	if (isString && object.length > 0 && !has.call(object, 0)) {
		for (var i = 0; i < object.length; ++i) {
			theKeys.push(String(i));
		}
	}

	if (isArguments && object.length > 0) {
		for (var j = 0; j < object.length; ++j) {
			theKeys.push(String(j));
		}
	} else {
		for (var name in object) {
			if (!(skipProto && name === 'prototype') && has.call(object, name)) {
				theKeys.push(String(name));
			}
		}
	}

	if (hasDontEnumBug) {
		var skipConstructor = equalsConstructorPrototypeIfNotBuggy(object);

		for (var k = 0; k < dontEnums.length; ++k) {
			if (!(skipConstructor && dontEnums[k] === 'constructor') && has.call(object, dontEnums[k])) {
				theKeys.push(dontEnums[k]);
			}
		}
	}
	return theKeys;
};

keysShim.shim = function shimObjectKeys() {
	if (Object.keys) {
		var keysWorksWithArguments = (function () {
			// Safari 5.0 bug
			return (Object.keys(arguments) || '').length === 2;
		}(1, 2));
		if (!keysWorksWithArguments) {
			var originalKeys = Object.keys;
			Object.keys = function keys(object) {
				if (isArgs(object)) {
					return originalKeys(slice.call(object));
				} else {
					return originalKeys(object);
				}
			};
		}
	} else {
		Object.keys = keysShim;
	}
	return Object.keys || keysShim;
};

module.exports = keysShim;


/***/ }),

/***/ "./node_modules/object-keys/isArguments.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var toStr = Object.prototype.toString;

module.exports = function isArguments(value) {
	var str = toStr.call(value);
	var isArgs = str === '[object Arguments]';
	if (!isArgs) {
		isArgs = str !== '[object Array]' &&
			value !== null &&
			typeof value === 'object' &&
			typeof value.length === 'number' &&
			value.length >= 0 &&
			toStr.call(value.callee) === '[object Function]';
	}
	return isArgs;
};


/***/ }),

/***/ "./node_modules/object.assign/implementation.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


// modified from https://github.com/es-shims/es6-shim
var keys = __webpack_require__("./node_modules/object-keys/index.js");
var bind = __webpack_require__("./node_modules/function-bind/index.js");
var canBeObject = function (obj) {
	return typeof obj !== 'undefined' && obj !== null;
};
var hasSymbols = __webpack_require__("./node_modules/has-symbols/shams.js")();
var toObject = Object;
var push = bind.call(Function.call, Array.prototype.push);
var propIsEnumerable = bind.call(Function.call, Object.prototype.propertyIsEnumerable);
var originalGetSymbols = hasSymbols ? Object.getOwnPropertySymbols : null;

module.exports = function assign(target, source1) {
	if (!canBeObject(target)) { throw new TypeError('target must be an object'); }
	var objTarget = toObject(target);
	var s, source, i, props, syms, value, key;
	for (s = 1; s < arguments.length; ++s) {
		source = toObject(arguments[s]);
		props = keys(source);
		var getSymbols = hasSymbols && (Object.getOwnPropertySymbols || originalGetSymbols);
		if (getSymbols) {
			syms = getSymbols(source);
			for (i = 0; i < syms.length; ++i) {
				key = syms[i];
				if (propIsEnumerable(source, key)) {
					push(props, key);
				}
			}
		}
		for (i = 0; i < props.length; ++i) {
			key = props[i];
			value = source[key];
			if (propIsEnumerable(source, key)) {
				objTarget[key] = value;
			}
		}
	}
	return objTarget;
};


/***/ }),

/***/ "./node_modules/object.assign/index.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var defineProperties = __webpack_require__("./node_modules/define-properties/index.js");

var implementation = __webpack_require__("./node_modules/object.assign/implementation.js");
var getPolyfill = __webpack_require__("./node_modules/object.assign/polyfill.js");
var shim = __webpack_require__("./node_modules/object.assign/shim.js");

var polyfill = getPolyfill();

defineProperties(polyfill, {
	getPolyfill: getPolyfill,
	implementation: implementation,
	shim: shim
});

module.exports = polyfill;


/***/ }),

/***/ "./node_modules/object.assign/polyfill.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var implementation = __webpack_require__("./node_modules/object.assign/implementation.js");

var lacksProperEnumerationOrder = function () {
	if (!Object.assign) {
		return false;
	}
	// v8, specifically in node 4.x, has a bug with incorrect property enumeration order
	// note: this does not detect the bug unless there's 20 characters
	var str = 'abcdefghijklmnopqrst';
	var letters = str.split('');
	var map = {};
	for (var i = 0; i < letters.length; ++i) {
		map[letters[i]] = letters[i];
	}
	var obj = Object.assign({}, map);
	var actual = '';
	for (var k in obj) {
		actual += k;
	}
	return str !== actual;
};

var assignHasPendingExceptions = function () {
	if (!Object.assign || !Object.preventExtensions) {
		return false;
	}
	// Firefox 37 still has "pending exception" logic in its Object.assign implementation,
	// which is 72% slower than our shim, and Firefox 40's native implementation.
	var thrower = Object.preventExtensions({ 1: 2 });
	try {
		Object.assign(thrower, 'xy');
	} catch (e) {
		return thrower[1] === 'y';
	}
	return false;
};

module.exports = function getPolyfill() {
	if (!Object.assign) {
		return implementation;
	}
	if (lacksProperEnumerationOrder()) {
		return implementation;
	}
	if (assignHasPendingExceptions()) {
		return implementation;
	}
	return Object.assign;
};


/***/ }),

/***/ "./node_modules/object.assign/shim.js":
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var define = __webpack_require__("./node_modules/define-properties/index.js");
var getPolyfill = __webpack_require__("./node_modules/object.assign/polyfill.js");

module.exports = function shimAssign() {
	var polyfill = getPolyfill();
	define(
		Object,
		{ assign: polyfill },
		{ assign: function () { return Object.assign !== polyfill; } }
	);
	return polyfill;
};


/***/ }),

/***/ "./node_modules/prop-types-exact/build/helpers/isPlainObject.js":
/***/ (function(module, exports) {

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

exports['default'] = isPlainObject;
function isPlainObject(x) {
  return x && (typeof x === 'undefined' ? 'undefined' : _typeof(x)) === 'object' && !Array.isArray(x);
}
module.exports = exports['default'];
//# sourceMappingURL=isPlainObject.js.map

/***/ }),

/***/ "./node_modules/prop-types-exact/build/index.js":
/***/ (function(module, exports, __webpack_require__) {

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports['default'] = forbidExtraProps;

var _object = __webpack_require__("./node_modules/object.assign/index.js");

var _object2 = _interopRequireDefault(_object);

var _has = __webpack_require__("./node_modules/has/src/index.js");

var _has2 = _interopRequireDefault(_has);

var _isPlainObject = __webpack_require__("./node_modules/prop-types-exact/build/helpers/isPlainObject.js");

var _isPlainObject2 = _interopRequireDefault(_isPlainObject);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var zeroWidthSpace = '\u200B';
var specialProperty = 'prop-types-exact: ' + zeroWidthSpace;
var semaphore = {};

function brand(fn) {
  return (0, _object2['default'])(fn, _defineProperty({}, specialProperty, semaphore));
}

function isBranded(value) {
  return value && value[specialProperty] === semaphore;
}

function forbidExtraProps(propTypes) {
  if (!(0, _isPlainObject2['default'])(propTypes)) {
    throw new TypeError('given propTypes must be an object');
  }
  if ((0, _has2['default'])(propTypes, specialProperty) && !isBranded(propTypes[specialProperty])) {
    throw new TypeError('Against all odds, you created a propType for a prop that uses both the zero-width space and our custom string - which, sadly, conflicts with `prop-types-exact`');
  }

  return (0, _object2['default'])({}, propTypes, _defineProperty({}, specialProperty, brand(function () {
    function forbidUnknownProps(props, _, componentName) {
      var unknownProps = Object.keys(props).filter(function (prop) {
        return !(0, _has2['default'])(propTypes, prop);
      });
      if (unknownProps.length > 0) {
        return new TypeError(String(componentName) + ': unknown props found: ' + String(unknownProps.join(', ')));
      }
      return null;
    }

    return forbidUnknownProps;
  }())));
}
module.exports = exports['default'];
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/webpack/buildin/harmony-module.js":
/***/ (function(module, exports) {

module.exports = function(originalModule) {
	if(!originalModule.webpackPolyfill) {
		var module = Object.create(originalModule);
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		Object.defineProperty(module, "exports", {
			enumerable: true,
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ "./pages/plan/SE.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("./node_modules/react/cjs/react.development.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_PlanLayout__ = __webpack_require__("./components/PlanLayout.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/pages/plan/SE.js";


(function () {
  var enterModule = __webpack_require__("./node_modules/react-hot-loader/index.js").enterModule;

  enterModule && enterModule(module);
})();



var SoftwareEngineering = function SoftwareEngineering() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__components_PlanLayout__["a" /* default */], {
    major: "Software Engineering",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  });
};

var _default = SoftwareEngineering;
/* harmony default export */ __webpack_exports__["default"] = (_default);
;

(function () {
  var reactHotLoader = __webpack_require__("./node_modules/react-hot-loader/index.js").default;

  var leaveModule = __webpack_require__("./node_modules/react-hot-loader/index.js").leaveModule;

  if (!reactHotLoader) {
    return;
  }

  reactHotLoader.register(SoftwareEngineering, "SoftwareEngineering", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/pages/plan/SE.js");
  reactHotLoader.register(_default, "default", "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/pages/plan/SE.js");
  leaveModule(module);
})();

;
    (function (Component, route) {
      if(!Component) return
      if (false) return
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/plan/SE")
  
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__("./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ }),

/***/ "./static/informatik1.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik1-a7111a755d5f4b66f3780a94579b307c.png";

/***/ }),

/***/ "./static/informatik2.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik2-60f5faa265ae6b244905ae33c6a3fba4.png";

/***/ }),

/***/ "./static/informatik3.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik3-2f4b3cc7881deff943fa9cad88a26e93.png";

/***/ }),

/***/ "./static/informatik4_IN.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_IN-3870366f55cd58e5b4442618495135da.png";

/***/ }),

/***/ "./static/informatik4_IS.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_IS-68612af83aab1415a6578239e9b30228.png";

/***/ }),

/***/ "./static/informatik4_MI.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_MI-ba51bfe4ed066b2d823f8fa3ba888033.png";

/***/ }),

/***/ "./static/informatik4_SE.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_SE-c8780eb97f863a5dc40069e65716a95f.png";

/***/ }),

/***/ "./static/informatik67_IN.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_IN-24765b238fdf495360ab3d0cde2e76df.png";

/***/ }),

/***/ "./static/informatik67_IS.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_IS-ded0c3bcedf67700d8c12e8c57879cd4.png";

/***/ }),

/***/ "./static/informatik67_MI.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_MI-b5a311b013c83025c9d86fdfe2b9ca8d.png";

/***/ }),

/***/ "./static/informatik67_SE.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_SE-8f3d6191eabd032913c5cd809e2006df.png";

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./pages/plan/SE.js");


/***/ })

},[6])
          return { page: comp.default }
        })
      ;
//# sourceMappingURL=SE.js.map