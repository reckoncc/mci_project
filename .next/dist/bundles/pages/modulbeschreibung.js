module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_head__ = __webpack_require__("next/head");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_head___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_head__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Navbar__ = __webpack_require__("./components/Navbar.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Layout.js";




var Layout = function Layout(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_head___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Informatik HS Aalen"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("link", {
    rel: "stylesheet",
    href: "https://bootswatch.com/4/simplex/bootstrap.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__Navbar__["a" /* default */], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    }
  }, props.children));
};

/* harmony default export */ __webpack_exports__["a"] = (Layout);

/***/ }),

/***/ "./components/ModuleTable.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/ModuleTable.js";


var ModuleTable = function ModuleTable(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 2
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("table", {
    className: "table",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("thead", {
    className: "thead-dark",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, "KNr"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Kurs / Modul"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, "Semester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, "CPs"))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tbody", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    }
  }, props.data.map(function (item) {
    if (item.type === 'module') {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
        className: "table-secondary",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 17
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        target: "_blank",
        href: item.url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        }
      }, item.KNr)), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        colSpan: "3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19
        }
      }, item.name));
    } else if (item.type === 'course') {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 24
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 25
        }
      }, item.KNr), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26
        }
      }, item.name), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 27
        }
      }, item.semester), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        }
      }, item.cp));
    }
  }))));
};

/* harmony default export */ __webpack_exports__["a"] = (ModuleTable);

/***/ }),

/***/ "./components/Navbar.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link__ = __webpack_require__("next/link");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_link__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Navbar.js";



var Navbar = function Navbar() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("nav", {
    className: "navbar navbar-expand navbar-dark bg-dark mb-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "navbar-brand",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Info"), "rmatik")), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "collapse navbar-collapse",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("ul", {
    className: "navbar-nav ml-auto",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("li", {
    className: "nav-item",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/modulbeschreibung",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "nav-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    }
  }, "Modulbeschreibung"))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("li", {
    className: "nav-item",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/formulare",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "nav-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    }
  }, "Formulare")))))));
};

/* harmony default export */ __webpack_exports__["a"] = (Navbar);

/***/ }),

/***/ "./components/data.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export news */
/* unused harmony export lectureFailures */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return moduleInfo; });
/* unused harmony export formData */
var news = [{
  'key': 1,
  'date': '2018-06-17',
  'header': 'Krawalle nach Deutschlandspiel',
  'content': 'Aufgrund des unglaublich vorhersehbaren Ausgangs des Deutschlandspiels gab es Krawalle am Campus Burren. Er bleibt deswegen für die nächsten beiden Tage geschlossen.'
}, {
  'key': 2,
  'date': '2018-06-14',
  'header': 'Ausbau der Parkplätze am Campus Burren',
  'content': "Nach gefühlt jahrhundertelangem Warten haben die verantwortlichen endlich auf die verzweifelten Schreie der Studenten nach mehr Parkplätzen reagiert. Nun wurde beschlossen das diese im kommenden Semester verdoppelt werden."
}];
var lectureFailures = [{
  key: 1,
  date: '2018-06-12',
  course: 'Software Projekt Management (Verlegung auf Mittwoch 15:45; Raum 1.01)'
}];
var moduleInfo = [{
  KNr: '57001',
  type: 'course',
  name: 'Grundlagen der Mathematik',
  cp: 5,
  extra_work: 'Tutorium, Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57002',
  type: 'module',
  name: 'Analysis',
  cp: 5,
  extra_work: 'Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57003',
  type: 'module',
  name: 'Rechnerarchitektur',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57004',
  type: 'module',
  name: 'Programmierung',
  cp: 10,
  extra_work: 'Testate, Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57104',
  type: 'course',
  name: 'Strukturierte Programmierung',
  cp: 5,
  extra_work: 'Testate',
  type_of_test: 'Written Exam'
}, //Medieninformatik erstes Semester
{
  KNr: '57021',
  type: 'module',
  name: 'Techniken des Mediendesigns',
  cp: 5,
  extra_work: 'Film, SVG-Projekt',
  type_of_test: 'Written Exam, Projects'
}, //Semester 2
//Teil von Programmierung
{
  KNr: '57201',
  type: 'course',
  name: 'Objektorientierte Programmierung',
  cp: 5,
  extra_work: 'Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57006',
  type: 'module',
  name: 'Diskrete Mathematik und lineare Algebra',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57007',
  type: 'module',
  name: 'Wahrscheinlichkeitstheorie und Statistik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57008',
  type: 'module',
  name: 'Algorithmen und Datenstrukturen 1',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 3
{
  KNr: '57010',
  type: 'module',
  name: 'Theoretische Informatik 1',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57011',
  type: 'module',
  name: 'Betriebssystheme',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57012',
  type: 'module',
  name: 'Algorithmen und Datenstrukturen 2',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57013',
  type: 'module',
  name: 'Objektorientierte Modellierung',
  cp: 5,
  extra_work: 'Praktikum',
  type_of_test: 'Written Exam'
}, //Semester 3 Allgemeine Informatik/Software Engineering
{
  KNr: '57017',
  type: 'module',
  name: 'Programmierpraktikum',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 3 IT-Sicherheit
{
  KNr: '57018',
  type: 'module',
  name: 'Sichere Programmierung',
  cp: 5,
  extra_work: 'Projekte',
  type_of_test: 'Projekte'
}, //Semester 4 alle
{
  KNr: '57901',
  type: 'module',
  name: 'Software Engineering',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57903',
  type: 'module',
  name: 'Rechnernetze',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 4 AGI
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57904',
  type: 'module',
  name: 'Mensch-Computer-Interaktion',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Written Exam'
}, //Semester 6 AGI
{
  KNr: '57907',
  type: 'module',
  name: 'Compilerbau',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57908',
  type: 'module',
  name: 'Vortgeschrittene Programmierung',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 AGI
{
  KNr: '57910',
  type: 'module',
  name: 'Cloud and Distributed Computing',
  cp: 5,
  extra_work: 'Project',
  type_of_test: 'Written Exam'
}, //Semester 4 ITS
{
  KNr: '57915',
  type: 'module',
  name: 'Betriebswirtschaftslehre',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57917',
  type: 'module',
  name: 'Sichere Hardware',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 6 ITS
{
  KNr: '57919',
  type: 'module',
  name: 'Datenschutz',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57920',
  type: 'module',
  name: 'Netzwerksicherheit',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 ITS
{
  KNr: '57704',
  type: 'course',
  name: 'Kryptographische Protokolle',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57021',
  type: 'module',
  name: 'Systemsicherheit',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Written Exam'
}, //Semester 4 MI
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57925',
  type: 'module',
  name: 'Virtuelle Realität und Animation',
  cp: 5,
  extra_work: 'Projekte',
  type_of_test: 'Projekte'
}, //Semester 6 MI
{
  KNr: '57907',
  type: 'module',
  name: 'Compilerbau',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57929',
  type: 'module',
  name: 'Bildverarbeitung und Musterekennung',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //semester 7 MI
{
  KNr: '57931',
  type: 'module',
  name: 'Computergraphik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57932',
  type: 'module',
  name: 'Spieleprogrammierung',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 4 SE
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57936',
  type: 'module',
  name: 'Komponenntenbasierte Softwaretechnik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 6 SE
{
  KNr: '57938',
  type: 'module',
  name: 'Mobile and Embedded Software Developement',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57939',
  type: 'module',
  name: 'Software Architecture',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 SE
{
  KNr: '57910',
  type: 'module',
  name: 'Cloud and Distributed Computing',
  cp: 5,
  extra_work: 'Project',
  type_of_test: 'Written Exam'
}, {
  KNr: '57939',
  type: 'module',
  name: 'Software Quality',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}];
var formData = {
  arbeit: [{
    name: 'Kolloquium',
    url: 'https://www.hs-aalen.de/uploads/mediapool/media/file/301/Pruefarb_kolloquiumsbesuch_STG-Informatik.pdf'
  }],
  praxis: [{
    name: 'Praxissemester Infoblatt',
    url: 'https://www.hs-aalen.de/de/courses/16/downloads'
  }],
  pruefung: [{
    name: 'Prüfungsplan SoSe2018',
    url: 'https://www.hs-aalen.de/de/pages/pruefungsplan'
  }],
  studiumGenerale: [{
    name: 'Informationen Studium Generale',
    url: 'https://www.hs-aalen.de/pages/career-center_faq'
  }],
  zeugnis: [{
    name: 'Anmeldung zum QIS online Formular',
    url: 'https://qis-studenten.htw-aalen.de/qisserverstud/rds?state=user&type=0'
  }],
  spo: [{
    name: 'Informationen zur SPO',
    url: 'https://www.hs-aalen.de/de/pages/studien-und-pruefungsordnungen-satzungen'
  }],
  sonstiges: [{
    name: 'Semesterplan SoSe2018',
    url: 'https://www.hs-aalen.de/semesters/9'
  }, {
    name: 'Semesterplan WS 2018/19',
    url: 'https://www.hs-aalen.de/semesters/10'
  }, {
    name: 'Zentraler Studierendenservice',
    url: 'https://www.hs-aalen.de/de/facilities/8/downloads'
  }]
};

/***/ }),

/***/ "./pages/modulbeschreibung.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_Layout__ = __webpack_require__("./components/Layout.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_ModuleTable__ = __webpack_require__("./components/ModuleTable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_data__ = __webpack_require__("./components/data.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/pages/modulbeschreibung.js";





var ModuleDescription = function ModuleDescription() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__components_Layout__["a" /* default */], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h1", {
    className: "display-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, "Modulbeschreibung"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__components_ModuleTable__["a" /* default */], {
    data: __WEBPACK_IMPORTED_MODULE_3__components_data__["a" /* moduleInfo */],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (ModuleDescription);

/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./pages/modulbeschreibung.js");


/***/ }),

/***/ "next/head":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/link":
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "react":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=modulbeschreibung.js.map