module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/InternshipInfo.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/InternshipInfo.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var InternshipInfo =
/*#__PURE__*/
function (_React$Component) {
  _inherits(InternshipInfo, _React$Component);

  function InternshipInfo(props) {
    _classCallCheck(this, InternshipInfo);

    return _possibleConstructorReturn(this, (InternshipInfo.__proto__ || Object.getPrototypeOf(InternshipInfo)).call(this, props));
  }

  _createClass(InternshipInfo, [{
    key: "render",
    value: function render() {
      var company;
      var place;
      var activity;
      var url;

      switch (this.props.major) {
        case 'Medieninformatik':
          company = 'Imsimity';
          place = 'St. Georgen im Schwarzwald';
          activity = 'Tätigkeiten in der Softwareentwicklung, Bereich 3D Modelierung, HTML 5, Unity.';
          url = 'http://imsimity.de/jobs.html';
          break;

        case 'IT-Sicherheit':
          company = 'Campusjäger GmbH';
          place = 'Karlsruhe';
          activity = 'Entwicklung von Microservices mit Python.';
          url = 'https://www.campusjaeger.de/jobs/praktikum-python-entwicklung-m-w-informatik-it-sicherheit-softwareentwicklung-karlsruhe';
          break;

        case 'Software Engineering':
          company = 'Robert Bosch GmbH';
          place = 'Abstatt';
          activity = 'Tätigkeiten im Bereich Toolentwicklung';
          url = 'https://www.praktikumsstellen.de/995010000004903410.html';
          break;

        case 'Allgemeine Informatik':
          company = 'Drägerwerk AG & Co. KgaA';
          place = 'Lübeck';
          activity = 'Unterstützung bei der Schnittstellenentwicklung und Dokumentation für technische Beschreibungen und Assentverwaltung';
          url = 'https://www.praktikumsstellen.de/995010000004903410.html';
          break;
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        }
      }, "Praxissemester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h5", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        }
      }, "Zum Beispiel bei ", __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("i", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        }
      }, company)), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h5", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        }
      }, "Aktivit\xE4t"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("p", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        }
      }, activity), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        href: url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        }
      }));
    }
  }]);

  return InternshipInfo;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (InternshipInfo);

/***/ }),

/***/ "./components/Layout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_head__ = __webpack_require__("next/head");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_head___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_head__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Navbar__ = __webpack_require__("./components/Navbar.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Layout.js";




var Layout = function Layout(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_head___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Informatik HS Aalen"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("link", {
    rel: "stylesheet",
    href: "https://bootswatch.com/4/simplex/bootstrap.min.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__Navbar__["a" /* default */], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    }
  }, props.children));
};

/* harmony default export */ __webpack_exports__["a"] = (Layout);

/***/ }),

/***/ "./components/LectureInfo.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__static_informatik1_png__ = __webpack_require__("./static/informatik1.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__static_informatik1_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__static_informatik1_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__static_informatik2_png__ = __webpack_require__("./static/informatik2.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__static_informatik2_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__static_informatik2_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__static_informatik3_png__ = __webpack_require__("./static/informatik3.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__static_informatik3_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__static_informatik3_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png__ = __webpack_require__("./static/informatik4_IN.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png__ = __webpack_require__("./static/informatik4_IS.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png__ = __webpack_require__("./static/informatik4_MI.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png__ = __webpack_require__("./static/informatik4_SE.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png__ = __webpack_require__("./static/informatik67_IN.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png__ = __webpack_require__("./static/informatik67_IS.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png__ = __webpack_require__("./static/informatik67_MI.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png__ = __webpack_require__("./static/informatik67_SE.png");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__InternshipInfo__ = __webpack_require__("./components/InternshipInfo.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/LectureInfo.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }














var LectureInfo =
/*#__PURE__*/
function (_React$Component) {
  _inherits(LectureInfo, _React$Component);

  function LectureInfo(props) {
    _classCallCheck(this, LectureInfo);

    return _possibleConstructorReturn(this, (LectureInfo.__proto__ || Object.getPrototypeOf(LectureInfo)).call(this, props));
  }

  _createClass(LectureInfo, [{
    key: "render",
    value: function render() {
      var img_url = '';
      var semester = '';

      switch (this.props.sem) {
        case 'sem1':
          img_url = __WEBPACK_IMPORTED_MODULE_1__static_informatik1_png___default.a;
          semester = 'Semester 1';
          break;

        case 'sem2':
          img_url = __WEBPACK_IMPORTED_MODULE_2__static_informatik2_png___default.a;
          semester = 'Semester 2';
          break;

        case 'sem3':
          img_url = __WEBPACK_IMPORTED_MODULE_3__static_informatik3_png___default.a;
          semester = 'Semester 3';
          break;

        case 'sem4':
          if (this.props.major === 'Allgemeine Informatik') {
            img_url = __WEBPACK_IMPORTED_MODULE_4__static_informatik4_IN_png___default.a;
          } else if (this.props.major === 'IT-Sicherheit') {
            img_url = __WEBPACK_IMPORTED_MODULE_5__static_informatik4_IS_png___default.a;
          } else if (this.props.major === 'Medieninformatik') {
            img_url = __WEBPACK_IMPORTED_MODULE_6__static_informatik4_MI_png___default.a;
          } else if (this.props.major === 'Software Engineering') {
            img_url = __WEBPACK_IMPORTED_MODULE_7__static_informatik4_SE_png___default.a;
          }

          semester = 'Semester 4';
          break;

        case 'sem5':
          img_url = '';
          break;

        case 'sem67':
          switch (this.props.major) {
            case 'Allgemeine Informatik':
              img_url = __WEBPACK_IMPORTED_MODULE_8__static_informatik67_IN_png___default.a;
              break;

            case 'IT-Sicherheit':
              img_url = __WEBPACK_IMPORTED_MODULE_9__static_informatik67_IS_png___default.a;
              break;

            case 'Medieninformatik':
              img_url = __WEBPACK_IMPORTED_MODULE_10__static_informatik67_MI_png___default.a;
              break;

            case 'Software Engineering':
              img_url = __WEBPACK_IMPORTED_MODULE_11__static_informatik67_SE_png___default.a;
          }

          semester = 'Semester 6/7';
          break;

        case 'wahl':
          semester = 'Wahlfächer';
      }

      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h3", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        }
      }, semester), this.props.sem === 'sem5' ? __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_12__InternshipInfo__["a" /* default */], {
        major: this.props.major,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        }
      }) : __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("img", {
        width: "800px",
        src: img_url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        }
      }));
    }
  }]);

  return LectureInfo;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (LectureInfo);

/***/ }),

/***/ "./components/ModuleTable.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/ModuleTable.js";


var ModuleTable = function ModuleTable(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 2
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("table", {
    className: "table",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("thead", {
    className: "thead-dark",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, "KNr"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Kurs / Modul"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, "Semester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("th", {
    scope: "col",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, "CPs"))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tbody", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    }
  }, props.data.map(function (item) {
    if (item.type === 'module') {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
        className: "table-secondary",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 17
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
        target: "_blank",
        href: item.url,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        }
      }, item.KNr)), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        colSpan: "3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19
        }
      }, item.name));
    } else if (item.type === 'course') {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("tr", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 24
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 25
        }
      }, item.KNr), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26
        }
      }, item.name), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 27
        }
      }, item.semester), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("td", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        }
      }, item.cp));
    }
  }))));
};

/* harmony default export */ __webpack_exports__["a"] = (ModuleTable);

/***/ }),

/***/ "./components/Navbar.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link__ = __webpack_require__("next/link");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_next_link___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_next_link__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Navbar.js";



var Navbar = function Navbar() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("nav", {
    className: "navbar navbar-expand navbar-dark bg-dark mb-4",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "container",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "navbar-brand",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("b", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Info"), "rmatik")), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "collapse navbar-collapse",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("ul", {
    className: "navbar-nav ml-auto",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("li", {
    className: "nav-item",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/modulbeschreibung",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "nav-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    }
  }, "Modulbeschreibung"))), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("li", {
    className: "nav-item",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 18
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1_next_link___default.a, {
    href: "/formulare",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 19
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    className: "nav-link",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    }
  }, "Formulare")))))));
};

/* harmony default export */ __webpack_exports__["a"] = (Navbar);

/***/ }),

/***/ "./components/PlanLayout.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Layout__ = __webpack_require__("./components/Layout.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Sidebar__ = __webpack_require__("./components/Sidebar.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ModuleTable__ = __webpack_require__("./components/ModuleTable.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__LectureInfo__ = __webpack_require__("./components/LectureInfo.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__data__ = __webpack_require__("./components/data.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/PlanLayout.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }




'';



var PlanLayout =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PlanLayout, _React$Component);

  function PlanLayout(props) {
    var _this;

    _classCallCheck(this, PlanLayout);

    _this = _possibleConstructorReturn(this, (PlanLayout.__proto__ || Object.getPrototypeOf(PlanLayout)).call(this, props));
    _this.state = {
      page: 'sem1'
    };
    _this.handleClick = _this.handleClick.bind(_assertThisInitialized(_this));
    return _this;
  }

  _createClass(PlanLayout, [{
    key: "handleClick",
    value: function handleClick(e) {
      e.preventDefault();
      this.setState({
        page: e.target.id
      });
    }
  }, {
    key: "render",
    value: function render() {
      return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__Layout__["a" /* default */], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 28
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 29
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("h1", {
        className: "display-4",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        }
      }, this.props.major), this.props.children, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("br", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32
        }
      }), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "row",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "col-md-3",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_2__Sidebar__["a" /* default */], {
        action: this.handleClick,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        }
      })), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
        className: "col-md-7",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        }
      }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_4__LectureInfo__["a" /* default */], {
        major: this.props.major,
        sem: this.state.page,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 38
        }
      })))));
    }
  }]);

  return PlanLayout;
}(__WEBPACK_IMPORTED_MODULE_0_react___default.a.Component);

/* harmony default export */ __webpack_exports__["a"] = (PlanLayout);

/***/ }),

/***/ "./components/Sidebar.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/components/Sidebar.js";


var Sidebar = function Sidebar(props) {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("div", {
    className: "list-group",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 2
    }
  }, __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem1",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 3
    }
  }, "Semester 1"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem2",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  }, "Semester 2"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem3",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    }
  }, "Semester 3"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem4",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    }
  }, "Semester 4"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem5",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    }
  }, "Praxissemester"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "sem67",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    }
  }, "Semester 6/7"), __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement("a", {
    onClick: props.action,
    id: "wahl",
    className: "list-group-item list-group-item-action",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    }
  }, "Wahlf\xE4cher"));
};

/* harmony default export */ __webpack_exports__["a"] = (Sidebar);

/***/ }),

/***/ "./components/data.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export news */
/* unused harmony export lectureFailures */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return moduleInfo; });
/* unused harmony export formData */
var news = [{
  'key': 1,
  'date': '2018-06-17',
  'header': 'Krawalle nach Deutschlandspiel',
  'content': 'Aufgrund des unglaublich vorhersehbaren Ausgangs des Deutschlandspiels gab es Krawalle am Campus Burren. Er bleibt deswegen für die nächsten beiden Tage geschlossen.'
}, {
  'key': 2,
  'date': '2018-06-14',
  'header': 'Ausbau der Parkplätze am Campus Burren',
  'content': "Nach gefühlt jahrhundertelangem Warten haben die verantwortlichen endlich auf die verzweifelten Schreie der Studenten nach mehr Parkplätzen reagiert. Nun wurde beschlossen das diese im kommenden Semester verdoppelt werden."
}];
var lectureFailures = [{
  key: 1,
  date: '2018-06-12',
  course: 'Software Projekt Management (Verlegung auf Mittwoch 15:45; Raum 1.01)'
}];
var moduleInfo = [{
  KNr: '57001',
  type: 'course',
  name: 'Grundlagen der Mathematik',
  cp: 5,
  extra_work: 'Tutorium, Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57002',
  type: 'module',
  name: 'Analysis',
  cp: 5,
  extra_work: 'Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57003',
  type: 'module',
  name: 'Rechnerarchitektur',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57004',
  type: 'module',
  name: 'Programmierung',
  cp: 10,
  extra_work: 'Testate, Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57104',
  type: 'course',
  name: 'Strukturierte Programmierung',
  cp: 5,
  extra_work: 'Testate',
  type_of_test: 'Written Exam'
}, //Medieninformatik erstes Semester
{
  KNr: '57021',
  type: 'module',
  name: 'Techniken des Mediendesigns',
  cp: 5,
  extra_work: 'Film, SVG-Projekt',
  type_of_test: 'Written Exam, Projects'
}, //Semester 2
//Teil von Programmierung
{
  KNr: '57201',
  type: 'course',
  name: 'Objektorientierte Programmierung',
  cp: 5,
  extra_work: 'Vortest',
  type_of_test: 'Written Exam'
}, {
  KNr: '57006',
  type: 'module',
  name: 'Diskrete Mathematik und lineare Algebra',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57007',
  type: 'module',
  name: 'Wahrscheinlichkeitstheorie und Statistik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57008',
  type: 'module',
  name: 'Algorithmen und Datenstrukturen 1',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 3
{
  KNr: '57010',
  type: 'module',
  name: 'Theoretische Informatik 1',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57011',
  type: 'module',
  name: 'Betriebssystheme',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57012',
  type: 'module',
  name: 'Algorithmen und Datenstrukturen 2',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57013',
  type: 'module',
  name: 'Objektorientierte Modellierung',
  cp: 5,
  extra_work: 'Praktikum',
  type_of_test: 'Written Exam'
}, //Semester 3 Allgemeine Informatik/Software Engineering
{
  KNr: '57017',
  type: 'module',
  name: 'Programmierpraktikum',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 3 IT-Sicherheit
{
  KNr: '57018',
  type: 'module',
  name: 'Sichere Programmierung',
  cp: 5,
  extra_work: 'Projekte',
  type_of_test: 'Projekte'
}, //Semester 4 alle
{
  KNr: '57901',
  type: 'module',
  name: 'Software Engineering',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57903',
  type: 'module',
  name: 'Rechnernetze',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 4 AGI
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57904',
  type: 'module',
  name: 'Mensch-Computer-Interaktion',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Written Exam'
}, //Semester 6 AGI
{
  KNr: '57907',
  type: 'module',
  name: 'Compilerbau',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57908',
  type: 'module',
  name: 'Vortgeschrittene Programmierung',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 AGI
{
  KNr: '57910',
  type: 'module',
  name: 'Cloud and Distributed Computing',
  cp: 5,
  extra_work: 'Project',
  type_of_test: 'Written Exam'
}, //Semester 4 ITS
{
  KNr: '57915',
  type: 'module',
  name: 'Betriebswirtschaftslehre',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57917',
  type: 'module',
  name: 'Sichere Hardware',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 6 ITS
{
  KNr: '57919',
  type: 'module',
  name: 'Datenschutz',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57920',
  type: 'module',
  name: 'Netzwerksicherheit',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 ITS
{
  KNr: '57704',
  type: 'course',
  name: 'Kryptographische Protokolle',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57021',
  type: 'module',
  name: 'Systemsicherheit',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Written Exam'
}, //Semester 4 MI
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57925',
  type: 'module',
  name: 'Virtuelle Realität und Animation',
  cp: 5,
  extra_work: 'Projekte',
  type_of_test: 'Projekte'
}, //Semester 6 MI
{
  KNr: '57907',
  type: 'module',
  name: 'Compilerbau',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57929',
  type: 'module',
  name: 'Bildverarbeitung und Musterekennung',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //semester 7 MI
{
  KNr: '57931',
  type: 'module',
  name: 'Computergraphik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57932',
  type: 'module',
  name: 'Spieleprogrammierung',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}, //Semester 4 SE
{
  KNr: '57902',
  type: 'module',
  name: 'Software Project Management',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57936',
  type: 'module',
  name: 'Komponenntenbasierte Softwaretechnik',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 6 SE
{
  KNr: '57938',
  type: 'module',
  name: 'Mobile and Embedded Software Developement',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, {
  KNr: '57939',
  type: 'module',
  name: 'Software Architecture',
  cp: 5,
  extra_work: '-',
  type_of_test: 'Written Exam'
}, //Semester 7 SE
{
  KNr: '57910',
  type: 'module',
  name: 'Cloud and Distributed Computing',
  cp: 5,
  extra_work: 'Project',
  type_of_test: 'Written Exam'
}, {
  KNr: '57939',
  type: 'module',
  name: 'Software Quality',
  cp: 5,
  extra_work: 'Projekt',
  type_of_test: 'Projekt'
}];
var formData = {
  arbeit: [{
    name: 'Kolloquium',
    url: 'https://www.hs-aalen.de/uploads/mediapool/media/file/301/Pruefarb_kolloquiumsbesuch_STG-Informatik.pdf'
  }],
  praxis: [{
    name: 'Praxissemester Infoblatt',
    url: 'https://www.hs-aalen.de/de/courses/16/downloads'
  }],
  pruefung: [{
    name: 'Prüfungsplan SoSe2018',
    url: 'https://www.hs-aalen.de/de/pages/pruefungsplan'
  }],
  studiumGenerale: [{
    name: 'Informationen Studium Generale',
    url: 'https://www.hs-aalen.de/pages/career-center_faq'
  }],
  zeugnis: [{
    name: 'Anmeldung zum QIS online Formular',
    url: 'https://qis-studenten.htw-aalen.de/qisserverstud/rds?state=user&type=0'
  }],
  spo: [{
    name: 'Informationen zur SPO',
    url: 'https://www.hs-aalen.de/de/pages/studien-und-pruefungsordnungen-satzungen'
  }],
  sonstiges: [{
    name: 'Semesterplan SoSe2018',
    url: 'https://www.hs-aalen.de/semesters/9'
  }, {
    name: 'Semesterplan WS 2018/19',
    url: 'https://www.hs-aalen.de/semesters/10'
  }, {
    name: 'Zentraler Studierendenservice',
    url: 'https://www.hs-aalen.de/de/facilities/8/downloads'
  }]
};

/***/ }),

/***/ "./pages/plan/SE.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react__ = __webpack_require__("react");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_react___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_react__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_PlanLayout__ = __webpack_require__("./components/PlanLayout.js");
var _jsxFileName = "/Users/Julian/Documents/edu/hs-aalen/IN_4/MCI/mci_project6/pages/plan/SE.js";



var SoftwareEngineering = function SoftwareEngineering() {
  return __WEBPACK_IMPORTED_MODULE_0_react___default.a.createElement(__WEBPACK_IMPORTED_MODULE_1__components_PlanLayout__["a" /* default */], {
    major: "Software Engineering",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    }
  });
};

/* harmony default export */ __webpack_exports__["default"] = (SoftwareEngineering);

/***/ }),

/***/ "./static/informatik1.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik1-a7111a755d5f4b66f3780a94579b307c.png";

/***/ }),

/***/ "./static/informatik2.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik2-60f5faa265ae6b244905ae33c6a3fba4.png";

/***/ }),

/***/ "./static/informatik3.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik3-2f4b3cc7881deff943fa9cad88a26e93.png";

/***/ }),

/***/ "./static/informatik4_IN.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_IN-3870366f55cd58e5b4442618495135da.png";

/***/ }),

/***/ "./static/informatik4_IS.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_IS-68612af83aab1415a6578239e9b30228.png";

/***/ }),

/***/ "./static/informatik4_MI.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_MI-ba51bfe4ed066b2d823f8fa3ba888033.png";

/***/ }),

/***/ "./static/informatik4_SE.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik4_SE-c8780eb97f863a5dc40069e65716a95f.png";

/***/ }),

/***/ "./static/informatik67_IN.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_IN-24765b238fdf495360ab3d0cde2e76df.png";

/***/ }),

/***/ "./static/informatik67_IS.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_IS-ded0c3bcedf67700d8c12e8c57879cd4.png";

/***/ }),

/***/ "./static/informatik67_MI.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_MI-b5a311b013c83025c9d86fdfe2b9ca8d.png";

/***/ }),

/***/ "./static/informatik67_SE.png":
/***/ (function(module, exports) {

module.exports = "/_next/static/images/informatik67_SE-8f3d6191eabd032913c5cd809e2006df.png";

/***/ }),

/***/ 6:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("./pages/plan/SE.js");


/***/ }),

/***/ "next/head":
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/link":
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "react":
/***/ (function(module, exports) {

module.exports = require("react");

/***/ })

/******/ });
//# sourceMappingURL=SE.js.map